# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://mbajuk@bitbucket.org/mbajuk/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/mbajuk/stroboskop/commits/bdb14754c396a0e6a2ed0c5af09ac70025eeaa75

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mbajuk/stroboskop/commits/e23607d00bf4d15e0b39f030efecf0790501b092

Naloga 6.3.2:
https://bitbucket.org/mbajuk/stroboskop/commits/24a5b75a5a35fc55cb859943c8c9954c912108f9

Naloga 6.3.3:
https://bitbucket.org/mbajuk/stroboskop/commits/b13165d38c30f50c1a226a8afb410f28e3a61806

Naloga 6.3.4:
https://bitbucket.org/mbajuk/stroboskop/commits/14d61f4dac93e6c9aa11ff3b6268cee27eec8e5f

Naloga 6.3.5:

```
git fetch
git checkout master
git merge izracun
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mbajuk/stroboskop/commits/75c0c4720b16fbffef997d8f73327418366a9ccb

Naloga 6.4.2:
https://bitbucket.org/mbajuk/stroboskop/commits/175dc3812d9731cf50f8050e968cd3468482452f

Naloga 6.4.3:
https://bitbucket.org/mbajuk/stroboskop/commits/4e78f2577592b676f8454e4953264d63016d4692

Naloga 6.4.4:
https://bitbucket.org/mbajuk/stroboskop/commits/6545311b597a18ab9baea3d8abf30840fb4f92b2